# GraphQL Menu

Adds menu helpers for GraphQL v3, that facilitates the query of submenus.

- Get child links and their entities while using `menuLinkContentById()`
- Get all menu link tree elements as `MenuLinkContent`,
including the disabled ones
- Add the enabled field to `MenuLink`
- Get menu link content linked entity if any with `linkedEntity`

## Examples

Base case: get menu link tree elements and nested links.
Fetches by default the menu link tree elements that have their link enabled.
Example use case: get as submenu for a given section.
The parent link is then fetched via the context or via `menuLinkContentById`

```graphql
{
  menuLinkContentById(id: "1") {
    # Parent
    ... MenuLinkContent
    # Children
    menuLinkTreeElements {
      ... MenuLink
      links {
        ... MenuLink
      }
    }
  }
}

fragment MenuLink on MenuLink {
  label
  url {
    path
  }
  enabled
}

fragment MenuLinkContent on MenuLinkContent {
  entityLabel
  entityUrl {
    path
  }
  enabled
}
```

Get all menu link tree elements, including the disabled ones.
Example use case: generate a site structure without wanting to
have all menu links displayed in the main menu.

```graphql
{
  menuLinkContentById(id: "1") {
    # Parent
    ... MenuLinkContent
    # Children
    menuLinkTreeElements(include_disabled: true) {
      entity {
        ... MenuLinkContent
        menuLinkTreeElements(include_disabled:true) {
          entity {
            ... MenuLinkContent
          }
        }
      }
    }
  }
}

fragment MenuLinkContent on MenuLinkContent {
  entityLabel
  entityUrl {
    path
  }
  enabled
}
```

Get linked entity.

```graphql
query {
  mainMenu: menuByName(name: "main") {
    name
    links {
      ...MenuItem
      links {
        ...MenuItem
      }
    }
  }
}

fragment MenuItem on MenuLink {
  label
  url {
    path
  }
  entity {
    ...on MenuLinkContent {
      linkedEntity(language_fallback: true, bypass_access_check: true) {
        entityId
        entityType
        ... on Node {
          status
          entityHasTranslation
        }
      }
    }
  }
}
```
