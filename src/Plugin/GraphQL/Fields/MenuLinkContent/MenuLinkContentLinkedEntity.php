<?php

namespace Drupal\graphql_menu\Plugin\GraphQL\Fields\MenuLinkContent;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\menu_link_content\MenuLinkContentInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves the menu link tree elements for a MenuLinkContent item.
 *
 * Arguments:
 *
 * Language fallback. By default false.
 * As in most cases we might want to get a specific translation.
 * In other cases it might be good to get the content from the source.
 *
 * Bypass access check. By default false.
 * This can be handy for cases where the entity status needs to be known
 * or only get the excerpt of the entity without the need to have access to it.
 *
 * @GraphQLField(
 *   id = "menu_link_content_linked_entity",
 *   name = "linkedEntity",
 *   description = @Translation("Target entity for a menu link content."),
 *   type = "Entity",
 *   secure = true,
 *   arguments = {
 *     "language_fallback" = "Boolean",
 *     "bypass_access_check" = "Boolean",
 *   },
 *   parents = {
 *     "MenuLinkContent",
 *   },
 *   response_cache_contexts = {"languages:language_url"}
 * )
 */
class MenuLinkContentLinkedEntity extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityRepositoryInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof MenuLinkContentInterface) {
      $url = $value->getUrlObject();
      // This can also be a field of Url.
      if ($url instanceof Url && !$url->isExternal() && $url->isRouted()) {
        try {
          $routeParts = explode('.', $url->getRouteName());
          if (isset($routeParts[1])) {
            $entityType = $routeParts[1];
            $parameters = $url->getRouteParameters();
            $storage = $this->entityTypeManager->getStorage($entityType);

            if ($entity = $storage->load($parameters[$entityType])) {
              $languageFallback = isset($args['language_fallback']) ? $args['language_fallback'] : FALSE;
              $bypassAccessCheck = isset($args['bypass_access_check']) ? $args['bypass_access_check'] : FALSE;
              $currentLangCode = $this->languageManager->getCurrentLanguage()->getId();

              if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
                if ($languageFallback) {
                  $entity = $this->entityRepository->getTranslationFromContext($entity);
                }
                elseif ($entity->hasTranslation($currentLangCode)) {
                  $entity = $entity->getTranslation($currentLangCode);
                }
                else {
                  yield (new CacheableValue(NULL))->addCacheTags(['4xx-response']);
                }
              }

              $access = $entity->access('view', NULL, TRUE);
              if ($access->isAllowed() || $bypassAccessCheck) {
                yield $entity;
              }
            }
          }
        }
        catch (\Throwable $throwable) {
          // Fail silently, if the entity type does not exist.
        }
      }
    }
  }

}
