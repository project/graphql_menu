<?php

namespace Drupal\graphql_menu\Plugin\GraphQL\Fields\MenuLinkContent;

use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Menu Link Content entity.
 *
 * @GraphQLField(
 *   id = "menu_link_content_entity",
 *   name = "entity",
 *   description = @Translation("Menu link content entity."),
 *   type = "MenuLinkContent",
 *   secure = true,
 *   parents = {
 *     "MenuLink",
 *   },
 *   response_cache_contexts = {"languages:language_interface"}
 * )
 */
class MenuLinkContentEntity extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityRepositoryInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityRepository = $container->get('entity.repository');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof MenuLinkTreeElement) {
      $uuid = $value->link->getDerivativeId();
      if ($uuid) {
        try {
          /** @var \Drupal\menu_link_content\MenuLinkContentInterface $entity */
          $entity = $this->entityRepository->loadEntityByUuid('menu_link_content', $uuid);
          if ($entity) {
            yield $this->entityRepository->getTranslationFromContext($entity);
          }
        }
        catch (\Throwable $throwable) {
          $this->messenger->addError($throwable->getMessage());
        }
      }
    }
  }

}
