<?php

namespace Drupal\graphql_menu\Plugin\GraphQL\Fields\MenuLinkContent;

use Drupal\Core\Menu\InaccessibleMenuLink;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\menu_link_content\MenuLinkContentInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves the menu link tree elements for a MenuLinkContent item.
 *
 * @GraphQLField(
 *   id = "menu_link_content_links",
 *   name = "menuLinkTreeElements",
 *   description = @Translation("Menu link content links."),
 *   type = "[MenuLink]",
 *   secure = true,
 *   arguments = {
 *     "include_disabled" = "Boolean"
 *   },
 *   parents = {
 *     "MenuLinkContent",
 *   },
 *   response_cache_contexts = {"languages:language_interface"}
 * )
 */
class MenuLinkContentLinks extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Menu\MenuLinkTreeInterface definition.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->menuTree = $container->get('menu.link_tree');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof MenuLinkContentInterface) {
      $includeDisabled = isset($args['include_disabled']) ? $args['include_disabled'] : FALSE;
      $menuName = $value->getMenuName();
      $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menuName);
      $parameters->setMinDepth(1);
      $parentMenuLinkId = 'menu_link_content:' . $value->uuid();
      $parameters->expandedParents = [];
      // Based on Menu Block logic.
      // Root the menu tree at the given ID.
      if ($parentMenuLinkId) {
        $parameters->setRoot($parentMenuLinkId);
        // Check if the tree contains links.
        $tree = $this->menuTree->load($menuName, $parameters);
        if (empty($tree)) {
          // Change the request to expand all children and limit the depth to
          // the immediate children of the root.
          $parameters->expandedParents = [];
          $parameters->setMinDepth(1);
          $parameters->setMaxDepth(1);
          // Re-load the tree.
          $tree = $this->menuTree->load($menuName, $parameters);
        }
      }
      // Load the tree if we haven't already.
      if (!isset($tree)) {
        $tree = $this->menuTree->load($menuName, $parameters);
      }
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      $tree = $this->menuTree->transform($tree, $manipulators);

      // Filter Inaccessible menu links.
      $tree = array_filter($tree, function ($value, $key) {
        return !$value->link instanceof InaccessibleMenuLink;
      }, ARRAY_FILTER_USE_BOTH);

      if ($includeDisabled) {
        foreach ($tree as $menuLinkTreeElement) {
          yield $menuLinkTreeElement;
        }
      }
      else {
        foreach ($tree as $menuLinkTreeElement) {
          if ($menuLinkTreeElement->link->isEnabled()) {
            yield $menuLinkTreeElement;
          }
        }
      }
    }
  }

}
