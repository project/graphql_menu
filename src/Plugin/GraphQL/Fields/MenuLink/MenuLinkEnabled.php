<?php

namespace Drupal\graphql_menu\Plugin\GraphQL\Fields\MenuLink;

use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve a menu link enabled property.
 *
 * @GraphQLField(
 *   id = "menu_link_enabled",
 *   secure = true,
 *   name = "enabled",
 *   description = @Translation("Menu link enabled."),
 *   type = "Boolean",
 *   parents = {
 *     "MenuLink"
 *   },
 *   response_cache_contexts = {"languages:language_interface"}
 * )
 */
class MenuLinkEnabled extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof MenuLinkTreeElement) {
      yield $value->link->isEnabled();
    }
  }

}
